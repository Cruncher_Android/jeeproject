import { TestBed } from '@angular/core/testing';

import { TestDetailsChapInfoService } from './test-details-chap-info.service';

describe('TestDetailsChapInfoService', () => {
  let service: TestDetailsChapInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestDetailsChapInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
