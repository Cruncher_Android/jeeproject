import { TestBed } from '@angular/core/testing';

import { ExistFileNameService } from './exist-file-name.service';

describe('ExistFileNameService', () => {
  let service: ExistFileNameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExistFileNameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
