import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { UserInfoService } from "./../../services/user-info.service";
import { Storage } from "@ionic/storage";
import { PopoverController, LoadingController } from "@ionic/angular";
import { LoadingServiceService } from "./../../services/loading-service.service";
import { HttpClient } from "@angular/common/http";
import { ApiServiceService } from "./../../services/api-service.service";
import { StorageServiceService } from './../../services/storage-service.service';

@Component({
  selector: "app-demo-registeration",
  templateUrl: "./demo-registeration.component.html",
  styleUrls: ["./demo-registeration.component.scss"],
})
export class DemoRegisterationComponent implements OnInit {
  demoInfo: {
    firstName: string;
    lastName: string;
    contactNo: string;
    email: string;
    distributerId: string;
    appId: string;
    date: string;
  };
  date: any;
  month: any;
  year: any;
  fullYear: any;
  constructor(
    private router: Router,
    private storage: Storage,
    private popoverController: PopoverController,
    private loadingController: LoadingController,
    private http: HttpClient,
    private databaseServiceService: DatabaseServiceService,
    private userInfoService: UserInfoService,
    private loadingServiceService: LoadingServiceService,
    private apiService: ApiServiceService,
    private storageService: StorageServiceService
  ) {}

  ngOnInit() {
    this.date = new Date().getDate();
    this.month = new Date().getMonth() + 1;
    this.fullYear = new Date().getFullYear();
    this.demoInfo = {
      contactNo: "",
      email: "",
      firstName: "",
      lastName: "",
      distributerId: "",
      appId: "",
      date: ""
    };
  }

  onSubmit() { 
    this.demoInfo.date =
        this.date.toString() +
        '-' +
        this.month.toString() +
        '-' +
        this.fullYear.toString()
        this.demoInfo.distributerId = "11";
        this.demoInfo.appId = "2";
        console.log("demo info", this.demoInfo);
    this.loadingServiceService.createLoading("Please wait").then((_) => {
      this.http
        .post(`${this.apiService.apiUrl}setDemo`, this.demoInfo)
        .subscribe((data) => {
          if (data) {
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  this.userInfoService
                    .insertIntoTableForDemo(this.demoInfo)
                    .then((_) => {
                      this.storage.ready().then((ready) => {
                        if (ready) {
                          this.storage.get("demoRegistered").then((data) => {
                            if (data == true) {
                              this.loadingController.dismiss().then((_) => {
                                this.router.navigateByUrl("/dashboard/home");
                                this.popoverController.dismiss();
                                this.storage.set("userLoggedIn", "demo");
                                this.storageService.userLoggedIn = "demo";
                                this.demoInfo = {
                                  contactNo: "",
                                  email: "",
                                  firstName: "",
                                  lastName: "",
                                  distributerId: "",
                                  appId: "",
                                  date: ""
                                };
                              });
                            } else {
                              let date = new Date().getDate().toString();
                              let month = (
                                new Date().getMonth() + 2
                              ).toString();
                              let year = new Date()
                                .getFullYear()
                                .toString()
                                .substr(2);
                              if (date.length == 1) date = "0" + date;
                              if (month.length == 1) month = "0" + month;
                              this.storage
                                .set("demoRegistered", true)
                                .then((_) => {
                                  this.storage.set(
                                    "expireDate",
                                    year + month + date
                                  );
                                  this.loadingController.dismiss().then((_) => {
                                    this.router.navigateByUrl(
                                      "/dashboard/home"
                                    );
                                    this.popoverController.dismiss();
                                    this.storage.set("userLoggedIn", "demo");
                                    this.storageService.userLoggedIn = "demo";
                                  });
                                });
                            }
                          });
                        }
                      });
                    });
                }
              });
          }
        });
    });
  }
}
