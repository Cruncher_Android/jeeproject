import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuestionPanelPage } from './question-panel.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionPanelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuestionPanelPageRoutingModule {}
