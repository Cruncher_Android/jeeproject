import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuestionPanelPage } from './question-panel.page';

describe('QuestionPanelPage', () => {
  let component: QuestionPanelPage;
  let fixture: ComponentFixture<QuestionPanelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionPanelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QuestionPanelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
