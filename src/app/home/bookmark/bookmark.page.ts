import { Component, OnInit } from '@angular/core';
import { SubjectsService } from 'src/app/services/subjects.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.page.html',
  styleUrls: ['./bookmark.page.scss'],
})
export class BookmarkPage implements OnInit {

  subjects: {subjectId: null,
    subjectName: '',
    imagePath: ''}[] = [];

  constructor(private subjectsService: SubjectsService,
              private storage: Storage) { 
    // console.log('bookmark page loaded');
  }

  ngOnInit() {
    this.storage.set('page', 'bookmark');
    this.subjectsService.loadSubjects().then( subjects => {
      this.subjects = subjects;
    })
  }
}
