import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestSeriesPageRoutingModule } from './test-series-routing.module';

import { TestSeriesPage } from './test-series.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestSeriesPageRoutingModule
  ],
  declarations: [TestSeriesPage]
})
export class TestSeriesPageModule {}
