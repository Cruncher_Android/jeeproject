import {
  OverallSummaryService,
  OverAllSummary,
} from "./../../services/overall-summary.service";
import { ViewTestInfoService } from "./../../services/view-test-info.service";
import { SaveTestInfoService } from "./../../services/save-test-info.service";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { QuestionBookmarkService } from "./../../services/question-bookmark.service";
import { Router, ActivatedRoute } from "@angular/router";
import { DatabaseServiceService } from "./../../services/database-service.service";
import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ModalController,
  ToastController,
  NavController,
  AlertController,
  PopoverController,
  Platform,
} from "@ionic/angular";
import { TestInfoComponent } from "../../components/test-info/test-info.component";
import { Chart } from "chart.js";
import { QuestionPanelPage } from "src/app/components/question-panel/question-panel.page";
import { StorageServiceService } from "./../../services/storage-service.service";

@Component({
  selector: "app-test",
  templateUrl: "./test.page.html",
  styleUrls: ["./test.page.scss"],
})
export class TestPage implements OnInit, OnDestroy {
  @ViewChild("pieChart1") pieChart1;
  @ViewChild("pieChart2") pieChart2;
  @ViewChild("pieChart3") pieChart3;
  pie1: any;
  pie2: any;
  pie3: any;

  // public columnChart1: GoogleChartInterface;

  index = 0;
  chaptersList = [];
  chcount = null;

  questions: {
    chapterId: number;
    questionCount: number;
  }[] = [];

  questionsList: {
    QUESTION_ID: number;
    QUESTION: string;
    OPTIONA: string;
    OPTIONB: string;
    OPTIONC: string;
    OPTIOND: string;
    ANSWER: string;
    HINT: string;
    QUESTION_YEAR: string;
    QUESTION_LEVEL: number;
    QUESTION_TYPE: number;
    ISQUESTIONASIMAGE: number;
    QUESTIONIMAGEPATH: string;
    ISOPTIONASIMAGE: number;
    OPTIONIMAGEPATH: string;
    ISHINTASIMAGE: number;
    HINTIMAGEPATH: string;
    CHAPTER_ID: number;
    SUBJECT_ID: string;
    ATTEMPTED_VALUE: number;
    ATTEMPTED_DEMO: number;
    NUMERICAL_ANSWER: string;
    UNIT: string;
    userAns: string;
    userNumericalAns: string;
    userUnit: string;
  }[] = [];

  testDetails: {
    testDate: string;
    subjectName: string;
    totalQue: number;
    corrMarks: number;
    inCorrMarks: number;
    obtainedMarks: number;
    phyObtainedMarks: number;
    chemObtainedMarks: number;
    mathsObtainedMarks: number;
    totalMarks: number;
    totalTime: number;
    attQueCount: number;
    nonAttQueCount: number;
    corrQueCount: number;
    inCorrQueCount: number;
    timeTakenMin: string;
    timeTakenSec: string;
    testType: string;
    testMode: string;
    questions: string;
    chapterList: string;
    manualCount: string;
    phyStart: number;
    chemStart: number;
    mathsStart: number;
    testSubmitted: boolean;
    questionStartsAt: number;
  };

  settings: {
    time: number;
    correctMarks: number;
    inCorrectMarks: number;
    questionCount: number;
    numericalCount: number;
  };

  cntManual: {
    cntEasy: number;
    cntMedium: number;
    cntHard: number;
    cntNum: number;
    cntNumNew: number;
    cntTheory: number;
    cntNonAtt: number;
    cntAtt: number;
    cntPrevAsk: number;
  };

  selectedOptionArr: {
    questionNo: number;
    option: string;
    userNumericalAns: string;
    userUnit: string;
  }[] = [];

  selectedOption: {
    questionNo: number;
    option: string;
    userNumericalAns: string;
    userUnit: string;
  };

  questionCount: number;
  option: string;
  inCorrOption: string;
  questonsPanel = false;
  count = 0;
  totalQuestions = [];
  page: string;
  chapterId: number;
  questionIds: {
    questionId: number;
    chapterId: number;
  }[] = [];

  testId: number;

  stopClockInterval;
  secs = 0;
  fmins;
  fsecs;

  selectionCleared: boolean = false;

  testType: string = "";
  subjectName: string = "";
  testMode: string = "";
  iconCount: number = 0;

  totalMarks: number = 0;
  outOfMarks: number = 0;
  corrMarks: number = 0;
  inCorrMarks: number = 0;

  totalPhyMarks: number = 0;
  totalChemMarks: number = 0;
  totalMathsMarks: number = 0;

  outOfPhyMarks: number = 1;
  outOfChemMarks: number = 1;
  outOfMathsMarks: number = 1;

  correctQuestions: number = 0;
  inCorrectQuestions: number = 0;
  skippedQuestions: number = 0;

  totalMins;
  totalSecs;
  testSubmitted: boolean = false;
  testView: boolean = true;

  removedBookmarkQustions = [];
  bookmarkQustions = [];
  bookmarkIcon: string = "bookmark-outline";

  allQuestions: {
    questionId: number;
    status: string;
  }[] = [];

  chapterWiseQueCount: {
    chapterNo: number;
    queCount: number;
  }[] = [];

  accuracy: number;
  score: number;
  correctPer: number;
  inCorrectPer: number;
  skippedPer: number;

  inGo: boolean = false;

  testGivenDate: string;
  questionStatus: string;

  manualStart = false;

  existingQuestions = [];

  questionPaperYear: number;

  testTime: number = 0;

  phyQuestionStarts: number = 0;
  chemQuestionStarts: number = 0;
  mathsQuestionStarts: number = 0;

  questionImage: boolean = true;
  questionImagePath: string = "";

  optionImage: boolean = true;
  optionImagePath: string = "";

  hintImage: boolean = true;
  hintImagePath: string = "";

  showInputBox: boolean = false;

  overAllSummary: OverAllSummary;
  totalPhyQue: number = 0;
  totalChemQue: number = 0;
  totalMathsQue: number = 0;

  loggedInType: string = "";

  firstName: string = "";
  lastName: string = "";

  answer: string = "";
  unit: string = "";

  constructor(
    private storage: Storage,
    private databaseServiceService: DatabaseServiceService,
    private router: Router,
    private modalController: ModalController,
    private questionBookmarkService: QuestionBookmarkService,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private navController: NavController,
    private alertController: AlertController,
    private status: StatusBar,
    private popoverController: PopoverController,
    private platform: Platform,
    private saveTestInfoService: SaveTestInfoService,
    private viewTestInfoService: ViewTestInfoService,
    private overallSummaryService: OverallSummaryService,
    public storageServiceService: StorageServiceService
  ) {
    this.platform.backButton.subscribeWithPriority(0, (processNextHandler) => {
      // console.log("in test back");
      if (this.router.url.startsWith("/test")) {
        if (
          this.page == "unit-test" ||
          this.page == "saved-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers"
        ) {
          if (this.testView == true && this.testSubmitted == false) {
          } else if (this.testView == true && this.testSubmitted == true) {
            this.testView = false;
          } else if (this.testView == false && this.testSubmitted == true) {
            if (!this.subjectName.startsWith("JEE")) {
              this.addRemoveBookmark();
            }
            if (this.page == "unit-test" || this.page == "paused-test")
              this.router.navigateByUrl("/dashboard/unit-test");
            // else if (this.page == "saved-test" || this.page == "paused-test")
            //   this.router.navigateByUrl("/dashboard/view-test");
            // else if (this.page == "question-papers")
            //   this.router.navigateByUrl("/dashboard/question-paper");
            else {
              this.navController.back();
            }
          }
        } else if (this.page == "bookmark") {
          if (this.inGo == true) {
            popoverController.dismiss();
          } else {
            this.navController.back();
          }
        }
        this.popoverController.dismiss();
      } else {
        processNextHandler();
      }
      // else if(this.router.url.startsWith('/dashboard')){
      //   navigator["app"].exitApp();
      // }
    });
  }
  ionViewDidEnter() {
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
  }

  ngOnInit() {
    // console.log("in test page");
    this.status.backgroundColorByHexString("#3880ff");

    this.firstName = this.storageServiceService.userInfo.firstName;
    this.lastName = this.storageServiceService.userInfo.lastName;

    this.selectedOption = {
      questionNo: null,
      option: "",
      userNumericalAns: "",
      userUnit: "",
    };

    this.settings = {
      time: 0,
      correctMarks: 0,
      inCorrectMarks: 0,
      questionCount: 0,
      numericalCount: 0,
    };

    this.cntManual = {
      cntEasy: 0,
      cntMedium: 0,
      cntHard: 0,
      cntNum: 0,
      cntNumNew: 0,
      cntTheory: 0,
      cntNonAtt: 0,
      cntAtt: 0,
      cntPrevAsk: 0,
    };

    this.storage.ready().then((ready) => {
      if (ready) {
        this.storage.get("userLoggedIn").then((data) => {
          this.loggedInType = data;
        });
        this.storage.get("page").then((result) => {
          this.page = result;
          // console.log("page", this.page);

          if (
            (this.page == "unit-test" || this.page == "paused-test") &&
            !this.subjectName.startsWith("JEE")
          ) {
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  this.overallSummaryService
                    .getOverallSummary(
                      this.loggedInType == "demo"
                        ? "overall_summary_demo"
                        : "overall_summary"
                    )
                    .then((result) => {
                      this.overAllSummary = result;
                      // console.log(
                      //   "loggedIn type in test page",
                      //   this.loggedInType
                      // );
                      // console.log(
                      //   "overAll Summary in test page",
                      //   this.overAllSummary
                      // );
                    });
                }
              });
          }
          if (this.page == "unit-test") {
            if (this.storageServiceService.userLoggedIn == true) {
              this.storage.get("defaultSettings").then((result) => {
                this.settings = result;
                this.secs = this.settings.time * 60;
                this.fmins = this.settings.time;
              });
            } else if (this.storageServiceService.userLoggedIn == "demo") {
              this.storage.get("defaultSettingsDemo").then((result) => {
                this.settings = result;
                this.secs = this.settings.time * 60;
                this.fmins = this.settings.time;
              });
            }

            // console.log(this.settings);
            this.storage.get("subjectName").then((result) => {
              this.subjectName = result;
              this.storage.get("selectedChapters").then((result) => {
                // console.log("result", result);
                this.chaptersList = result;
                this.storage.get("subject-test").then((result) => {
                  if (result == true) {
                    this.testType = "SUBJECT TEST";
                  } else {
                    if (
                      this.chaptersList.length == 1 &&
                      this.subjectName != "PCM"
                    )
                      this.testType = "CHAPTER TEST";
                    else if (
                      this.chaptersList.length > 1 &&
                      this.subjectName != "PCM"
                    )
                      this.testType = "UNIT TEST";
                    else if (this.subjectName == "PCM")
                      this.testType = "GROUP TEST";
                  }
                });
                this.storage.get("testMode").then((result) => {
                  this.testMode = result;
                  if (this.testMode == "automatic") {
                    this.getQuestionsForUnitTest();
                  } else if (this.testMode == "manual") {
                    this.storage.get("cntManual").then((result) => {
                      this.cntManual = result;
                      this.getQuestionsForManualEasy();
                      // this.getQuestionsForManualMedium();
                      // this.getQuestionsForManualHard();
                      // this.getQuestionsForManualAttempted();
                      // this.getQuestionsForManualNonAttempted();
                      // this.getQuestionsForManualNumerical();
                      // this.getQuestionsForManualTheory();
                      // this.getQuestionsForManualPrevAsk();
                    });
                  }
                });
                this.chcount = this.chaptersList.length;
                // console.log(this.settings);

                this.questionBookmarkService
                  .getBookmarkQueId(
                    this.chaptersList,
                    this.loggedInType == "demo"
                      ? "question_bookmark_demo"
                      : "question_bookmark"
                  )
                  .then((questions) => {
                    this.bookmarkQustions = questions;
                    // console.log("bookmark questions", this.bookmarkQustions);
                  });
              });
            });
          } else if (this.page == "bookmark") {
            this.storage.get("bookChapterId").then((data) => {
              let chapterId = data;
              // this.activatedRoute.paramMap.subscribe((chapId) => {
              //   let chapterId = chapId.get("id");
              // this.chapterId = this.activatedRoute.snapshot.paramMap.get('chapterId');
              this.questionBookmarkService
                .getBookmarkQueId(
                  chapterId,
                  this.loggedInType == "demo"
                    ? "question_bookmark_demo"
                    : "question_bookmark"
                )
                .then((result) => {
                  this.questionIds = result;
                  // console.log("id in test page", this.questionIds);
                  let values = [];
                  this.questionIds.map((ids) => {
                    values.push(ids.questionId);
                  });
                  // let values = this.questionIds.questionId .toString();
                  // console.log("values", values);
                  this.getQuestions(values, []);
                });
            });
          } else if (this.page == "saved-test") {
            this.testView = false;
            this.testSubmitted = true;
            this.testId = parseInt(
              this.activatedRoute.snapshot.paramMap.get("id")
            );
            // this.activatedRoute.paramMap.subscribe(id => {
            //   this.testId = parseInt(id.get('id'))
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  this.saveTestInfoService
                    .getTestDetailsForTest(
                      this.testId,
                      this.loggedInType == "demo"
                        ? "save_test_info_demo"
                        : "save_test_info"
                    )
                    .then((data) => {
                      this.testDetails = data;
                      // console.log("test details", this.testDetails);
                      this.subjectName = this.testDetails.subjectName;
                      this.totalMarks = this.testDetails.obtainedMarks;
                      this.totalPhyMarks = this.testDetails.phyObtainedMarks;
                      this.totalChemMarks = this.testDetails.chemObtainedMarks;
                      this.totalMathsMarks = this.testDetails.mathsObtainedMarks;
                      this.outOfMarks = this.testDetails.totalMarks;
                      this.totalMins = this.testDetails.timeTakenMin;
                      this.totalSecs = this.testDetails.timeTakenSec;
                      this.corrMarks = this.testDetails.corrMarks;
                      this.inCorrMarks = this.testDetails.inCorrMarks;
                      this.correctQuestions = this.testDetails.corrQueCount;
                      this.inCorrectQuestions = this.testDetails.inCorrQueCount;
                      this.phyQuestionStarts = this.testDetails.phyStart;
                      this.chemQuestionStarts = this.testDetails.chemStart;
                      this.mathsQuestionStarts = this.testDetails.mathsStart;
                      this.skippedQuestions =
                        this.testDetails.totalQue -
                        (this.correctQuestions + this.inCorrectQuestions);
                      this.testGivenDate =
                        this.testDetails.testDate.slice(0, 2) +
                        "-" +
                        this.testDetails.testDate.slice(2, 4) +
                        "-" +
                        this.testDetails.testDate.slice(4);
                        
                      this.createPieCharts();
                      this.viewTestInfoService
                        .getUserAns(
                          this.testId,
                          this.loggedInType == "demo"
                            ? "view_test_info_demo"
                            : "view_test_info"
                        )
                        .then((data) => {
                          let userAnsArray = [];
                          data.map((userAnswers) => {
                            userAnsArray.push({
                              queId: userAnswers.queId,
                              userAns: userAnswers.userAns,
                              userNumericalAns: userAnswers.userNumericalAns,
                              userUnit: userAnswers.userUnit,
                            });
                          });
                          // console.log("userAns", userAnsArray);
                          let values = [];
                          userAnsArray.map((userAnswer) => {
                            values.push(userAnswer.queId);
                          });
                          this.getQuestions(values, userAnsArray);
                        });
                    });
                }
              });
            // })
          } else if (this.page == "paused-test") {
            // console.log("testId", this.testId);
            this.testView = true;
            this.testSubmitted = false;
            this.testId = parseInt(
              this.activatedRoute.snapshot.paramMap.get("id")
            );
            this.databaseServiceService
              .getDatabaseState()
              .subscribe((ready) => {
                if (ready) {
                  this.saveTestInfoService
                    .getTestDetailsForTest(
                      this.testId,
                      this.loggedInType == "demo"
                        ? "save_test_info_demo"
                        : "save_test_info"
                    )
                    .then((data) => {
                      this.testDetails = data;
                      // console.log("testDetails", this.testDetails);
                      this.settings = {
                        correctMarks: this.testDetails.corrMarks,
                        inCorrectMarks: this.testDetails.inCorrMarks,
                        questionCount: this.testDetails.totalQue,
                        time: this.testDetails.totalTime,
                        numericalCount: 0,
                      };
                      this.subjectName = this.testDetails.subjectName;
                      this.testType = this.testDetails.testType;
                      this.testMode = this.testDetails.testMode;
                      this.phyQuestionStarts = this.testDetails.phyStart;
                      this.chemQuestionStarts = this.testDetails.chemStart;
                      this.mathsQuestionStarts = this.testDetails.mathsStart;
                      this.chaptersList = this.testDetails.chapterList.split(
                        ","
                      );
                      this.questions = JSON.parse(this.testDetails.questions);
                      this.cntManual = JSON.parse(this.testDetails.manualCount);
                      this.index = this.testDetails.questionStartsAt;
                      // console.log("chapterList", this.chaptersList);
                      // console.log("questions", this.questions);
                      // console.log("manual count", this.cntManual);
                    });
                  this.viewTestInfoService
                    .getUserAns(
                      this.testId,
                      this.loggedInType == "demo"
                        ? "view_test_info_demo"
                        : "view_test_info"
                    )
                    .then((data) => {
                      let userAnsArray = [];
                      data.map((userAnswers) => {
                        userAnsArray.push({
                          queId: userAnswers.queId,
                          userAns: userAnswers.userAns,
                          userNumericalAns: userAnswers.userNumericalAns,
                          userUnit: userAnswers.userUnit,
                        });
                      });
                      // console.log("userAns", userAnsArray);
                      let values = [];
                      userAnsArray.map((userAnswer) => {
                        values.push(userAnswer.queId);
                      });
                      this.getQuestions(values, userAnsArray);
                    });
                }
              });
          } else if (this.page == "question-papers") {
            this.questionPaperYear = parseInt(
              this.activatedRoute.snapshot.paramMap.get("id")
            );
            this.subjectName = "JEE-" + this.questionPaperYear.toString();
            this.testType = this.subjectName;
            this.databaseServiceService
              .getDataBase()
              .executeSql(
                `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,
                ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR,ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,
                ISHINTASIMAGE,HINTIMAGEPATH FROM question_paper_pcm_info WHERE ATTEMPTEDVALUE = ? AND SUBJECT_ID=1 AND QUESTION_YEAR=?`,
                [0, this.questionPaperYear]
              )
              .then((rs) => {
                // console.log("physics questions", rs);
                this.questionsList = [];
                for (var i = 0; i < rs.rows.length; i++) {
                  this.questionsList.push({
                    QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                    QUESTION: rs.rows.item(i).QUESTION,
                    OPTIONA: rs.rows.item(i).OPTIONA,
                    OPTIONB: rs.rows.item(i).OPTIONB,
                    OPTIONC: rs.rows.item(i).OPTIONC,
                    OPTIOND: rs.rows.item(i).OPTIOND,
                    ANSWER: rs.rows.item(i).ANSWER,
                    HINT: rs.rows.item(i).HINT,
                    QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                    QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                    QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                    ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                    QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                    ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                    OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                    ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                    HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                    CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                    SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                    ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                    ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                    NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                    UNIT: rs.rows.item(i).UNIT,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                  });
                }
                this.chemQuestionStarts = this.questionsList.length;
                this.outOfPhyMarks = this.chemQuestionStarts * 4;
                // console.log("outOfPhy", this.outOfPhyMarks);
              });

            this.databaseServiceService
              .getDataBase()
              .executeSql(
                `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,
                  ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR,ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,
                  ISHINTASIMAGE,HINTIMAGEPATH FROM question_paper_pcm_info WHERE ATTEMPTEDVALUE = ? AND SUBJECT_ID=2 AND QUESTION_YEAR=?`,
                [0, this.questionPaperYear]
              )
              .then((rs) => {
                // console.log("chem questions", rs);
                for (var i = 0; i < rs.rows.length; i++) {
                  this.questionsList.push({
                    QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                    QUESTION: rs.rows.item(i).QUESTION,
                    OPTIONA: rs.rows.item(i).OPTIONA,
                    OPTIONB: rs.rows.item(i).OPTIONB,
                    OPTIONC: rs.rows.item(i).OPTIONC,
                    OPTIOND: rs.rows.item(i).OPTIOND,
                    ANSWER: rs.rows.item(i).ANSWER,
                    HINT: rs.rows.item(i).HINT,
                    QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                    QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                    QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                    ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                    QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                    ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                    OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                    ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                    HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                    CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                    SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                    ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                    ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                    NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                    UNIT: rs.rows.item(i).UNIT,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                  });
                }
                this.mathsQuestionStarts = this.questionsList.length;
                this.outOfChemMarks =
                  (this.questionsList.length - this.chemQuestionStarts) * 4;
                // console.log("outOfChem", this.outOfChemMarks);
              });

            this.databaseServiceService
              .getDataBase()
              .executeSql(
                `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,
                    ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR,ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,
                    ISHINTASIMAGE,HINTIMAGEPATH FROM question_paper_pcm_info WHERE ATTEMPTEDVALUE = ? AND SUBJECT_ID=3 AND QUESTION_YEAR=?`,
                [0, this.questionPaperYear]
              )
              .then((rs) => {
                // console.log("maths questions", rs);
                for (var i = 0; i < rs.rows.length; i++) {
                  this.questionsList.push({
                    QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                    QUESTION: rs.rows.item(i).QUESTION,
                    OPTIONA: rs.rows.item(i).OPTIONA,
                    OPTIONB: rs.rows.item(i).OPTIONB,
                    OPTIONC: rs.rows.item(i).OPTIONC,
                    OPTIOND: rs.rows.item(i).OPTIOND,
                    ANSWER: rs.rows.item(i).ANSWER,
                    HINT: rs.rows.item(i).HINT,
                    QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                    QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                    QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                    ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                    QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                    ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                    OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                    ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                    HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                    CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                    SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                    ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                    ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                    NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                    UNIT: rs.rows.item(i).UNIT,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                  });
                }
                this.outOfMathsMarks =
                  (this.questionsList.length - this.mathsQuestionStarts) * 4;
                // console.log("outOfMaths", this.outOfMathsMarks);
                // console.log("questionList", this.questionsList);
                this.questionCount = this.questionsList.length;
                this.storage.ready().then((ready) => {
                  if (ready) {
                    this.storage.get("testTime").then((data) => {
                      if (data != 0) this.testTime = data;
                      else this.testTime = 180;
                      this.secs = this.testTime * 60;
                      this.storage.set("testTime", 0);

                      this.settings = {
                        correctMarks: 4,
                        inCorrectMarks: 1,
                        questionCount: this.questionCount,
                        time: this.testTime,
                        numericalCount: 0,
                      };
                    });
                  }
                });
                this.showTest();
                this.onReady();
              });
          }
        });
      }
    });
  }

  getQuestions(values, userAnsArray) {
    let tableName: string = "question_info";
    if (this.subjectName.startsWith("JEE"))
      tableName = "question_paper_pcm_info";
    // console.log("values in getQuestions", values);
    this.databaseServiceService
      .getDataBase()
      .executeSql(
        `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, 
    HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,
    ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM ${tableName} WHERE QUESTION_ID IN (${values.toString()})`,
        []
      )
      .then((rs) => {
        this.questionsList = [];
        // console.log('bookmarked Questions', rs);
        for (var i = 0; i < rs.rows.length; i++) {
          this.questionsList.push({
            QUESTION_ID: rs.rows.item(i).QUESTION_ID,
            QUESTION: rs.rows.item(i).QUESTION,
            OPTIONA: rs.rows.item(i).OPTIONA,
            OPTIONB: rs.rows.item(i).OPTIONB,
            OPTIONC: rs.rows.item(i).OPTIONC,
            OPTIOND: rs.rows.item(i).OPTIOND,
            ANSWER: rs.rows.item(i).ANSWER,
            HINT: rs.rows.item(i).HINT,
            QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
            QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
            QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
            ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
            QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
            ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
            OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
            ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
            HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
            CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
            SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
            ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
            ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
            NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
            UNIT: rs.rows.item(i).UNIT,
            userAns: "",
            userNumericalAns: "",
            userUnit: "",
          });
        }
        // console.log("questionsList1", this.questionsList);
        this.questionCount = rs.rows.length;
        if (this.page == "bookmark") this.showTest();
        if (this.page == "saved-test" || this.page == "paused-test") {
          let selectedQuestion;
          this.questionsList.map((questions) => {
            selectedQuestion = userAnsArray.find(
              ({ queId }) => queId == questions.QUESTION_ID
            );
            questions.userAns = selectedQuestion.userAns;
            questions.userNumericalAns = selectedQuestion.userNumericalAns;
            questions.userUnit = selectedQuestion.userUnit;
          });
          let questionIds = [];
          this.questionsList.map((question) => {
            questionIds.push(question.QUESTION_ID);
          });
          if (!this.subjectName.startsWith("JEE")) {
            this.questionBookmarkService
              .selectId(
                questionIds,
                this.loggedInType == "demo"
                  ? "question_bookmark_demo"
                  : "question_bookmark"
              )
              .then((questions) => {
                this.bookmarkQustions = questions;
                // console.log(
                //   "bookmark questions save test",
                //   this.bookmarkQustions
                // );
              });
          }
        }
        if (this.page == "paused-test") {
          this.selectedOptionArr = [];
          this.questionsList.map((questions) => {
            if (questions.QUESTION_TYPE != 2 && questions.userAns != "") {
              this.selectedOptionArr.push({
                questionNo: questions.QUESTION_ID,
                option: questions.userAns,
                userNumericalAns: questions.userNumericalAns,
                userUnit: questions.userUnit,
              });
            }
          });
          this.answer = this.questionsList[this.index].userNumericalAns;
          this.unit = this.questionsList[this.index].userUnit;
          this.funSelectedOption();
          this.showTest();
          this.onReady();
        }
        // console.log("questionsList", this.questionsList);
        this.outOfPhyMarks = this.chemQuestionStarts * 4;
        this.outOfChemMarks =
          (this.mathsQuestionStarts - this.chemQuestionStarts) * 4;
        this.outOfMathsMarks =
          (this.questionsList.length - this.mathsQuestionStarts) * 4;
      });
  }

  getQuestionsForUnitTest() {
    let questionCount = Math.floor(
      this.settings.questionCount / this.chaptersList.length
    );
    let remainder = this.settings.questionCount % this.chaptersList.length;
    this.chaptersList = this.chaptersList.sort((a, b) => a - b);
    // console.log("chapterList", this.chaptersList);
    this.chaptersList.map((chapter) => {
      this.questions.push({
        chapterId: chapter,
        questionCount: questionCount,
      });
    });
    if (remainder > 0) {
      for (let i = 0; i < remainder; i++) {
        this.questions[i].questionCount++;
      }
    }
    // console.log("questioncount", this.questions);
    for (let j = 0; j < this.questions.length; j++) {
      // console.log("j", j);
      this.databaseServiceService
        .getDataBase()
        .executeSql(
          `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
      CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
      question_info WHERE ${
        this.loggedInType == "demo"
          ? "ATTEMPTED_DEMO = 0"
          : "ATTEMPTEDVALUE = 0"
      } AND QUESTION_TYPE != 2 AND CHAPTER_ID = ? limit ?`,
          [this.questions[j].chapterId, this.questions[j].questionCount]
        )
        .then((rs) => {
          let questionCount: number = 0;
          for (let i = 0; i < rs.rows.length; i++) {
            questionCount += 1;
            this.questionsList.push({
              QUESTION_ID: rs.rows.item(i).QUESTION_ID,
              QUESTION: rs.rows.item(i).QUESTION,
              OPTIONA: rs.rows.item(i).OPTIONA,
              OPTIONB: rs.rows.item(i).OPTIONB,
              OPTIONC: rs.rows.item(i).OPTIONC,
              OPTIOND: rs.rows.item(i).OPTIOND,
              ANSWER: rs.rows.item(i).ANSWER,
              HINT: rs.rows.item(i).HINT,
              QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
              QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
              QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
              ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
              QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
              ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
              OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
              ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
              HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
              CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
              SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
              ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
              ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
              NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
              UNIT: rs.rows.item(i).UNIT,
              userAns: "",
              userNumericalAns: "",
              userUnit: "",
            });
          }
          // console.log("question count for loop", questionCount);
          if (questionCount < this.questions[j].questionCount) {
            this.databaseServiceService
              .getDataBase()
              .executeSql(
                `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
              CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
              question_info WHERE ${
                this.loggedInType == "demo"
                  ? "ATTEMPTED_DEMO = 1"
                  : "ATTEMPTEDVALUE = 1"
              } AND QUESTION_TYPE != 2 AND CHAPTER_ID = ? limit ?`,
                [
                  this.questions[j].chapterId,
                  this.questions[j].questionCount - questionCount,
                ]
              )
              .then((rs) => {
                this.questionCount += rs.rows.length;
                for (let i = 0; i < rs.rows.length; i++) {
                  this.questionsList.push({
                    QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                    QUESTION: rs.rows.item(i).QUESTION,
                    OPTIONA: rs.rows.item(i).OPTIONA,
                    OPTIONB: rs.rows.item(i).OPTIONB,
                    OPTIONC: rs.rows.item(i).OPTIONC,
                    OPTIOND: rs.rows.item(i).OPTIOND,
                    ANSWER: rs.rows.item(i).ANSWER,
                    HINT: rs.rows.item(i).HINT,
                    QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                    QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                    QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                    ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                    QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                    ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                    OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                    ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                    HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                    CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                    SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                    ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                    ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                    NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                    UNIT: rs.rows.item(i).UNIT,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                  });
                }
                // this.questionsList = this.questionsList.sort(
                //   (a, b) => a.QUESTION_ID - b.QUESTION_ID
                // );
                // if (j == this.questions.length - 1) this.showTest();
              });
          }
          if (j == this.questions.length - 1) {
            // console.log("for loop completed");
            // this.questionsList = this.questionsList.sort(
            //   (a, b) => a.QUESTION_ID - b.QUESTION_ID
            // );
            // this.questionCount = this.questionsList.length;
            // console.log("questionList", this.questionsList);
            // console.log("que", this.questionsList.length);
            // this.showTest();
            // this.onReady();
            this.getNumericalQuestions();
          }
        });
    }
  }

  getNumericalQuestions() {
    let numericalQuestions: {
      chapterId: number;
      questionCount: number;
    }[] = [];
    // console.log("numerical questions");
    let questionCount = Math.floor(
      this.settings.numericalCount / this.chaptersList.length
    );
    let remainder = this.settings.numericalCount % this.chaptersList.length;
    this.chaptersList = this.chaptersList.sort((a, b) => a - b);
    // console.log("chapterList", this.chaptersList);
    this.chaptersList.map((chapter) => {
      numericalQuestions.push({
        chapterId: chapter,
        questionCount: questionCount,
      });
    });
    if (remainder > 0) {
      for (let i = 0; i < remainder; i++) {
        numericalQuestions[i].questionCount++;
      }
    }
    // console.log("questioncount", numericalQuestions);
    for (let j = 0; j < numericalQuestions.length; j++) {
      // console.log("j", j);
      this.databaseServiceService
        .getDataBase()
        .executeSql(
          `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
      CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
      question_info WHERE ${
        this.loggedInType == "demo"
          ? "ATTEMPTED_DEMO = 0"
          : "ATTEMPTEDVALUE = 0"
      } AND QUESTION_TYPE = 2 AND CHAPTER_ID = ? limit ?`,
          [numericalQuestions[j].chapterId, numericalQuestions[j].questionCount]
        )
        .then((rs) => {
          let questionCount: number = 0;
          // console.log("question count numerical", this.questions);
          for (let i = 0; i < rs.rows.length; i++) {
            // console.log(this.questions[i].questionCount)
            this.questions[j].questionCount += 1;
            // this.questions[i].questionCount + 1;
            questionCount += 1;
            this.questionsList.push({
              QUESTION_ID: rs.rows.item(i).QUESTION_ID,
              QUESTION: rs.rows.item(i).QUESTION,
              OPTIONA: rs.rows.item(i).OPTIONA,
              OPTIONB: rs.rows.item(i).OPTIONB,
              OPTIONC: rs.rows.item(i).OPTIONC,
              OPTIOND: rs.rows.item(i).OPTIOND,
              ANSWER: rs.rows.item(i).ANSWER,
              HINT: rs.rows.item(i).HINT,
              QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
              QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
              QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
              ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
              QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
              ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
              OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
              ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
              HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
              CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
              SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
              ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
              ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
              NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
              UNIT: rs.rows.item(i).UNIT,
              userAns: "",
              userNumericalAns: "",
              userUnit: "",
            });
          }
          // console.log("question count for loop", questionCount);
          if (questionCount < numericalQuestions[j].questionCount) {
            this.databaseServiceService
              .getDataBase()
              .executeSql(
                `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
              CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
              question_info WHERE ${
                this.loggedInType == "demo"
                  ? "ATTEMPTED_DEMO = 1"
                  : "ATTEMPTEDVALUE = 1"
              } AND QUESTION_TYPE = 2 AND CHAPTER_ID = ? limit ?`,
                [
                  numericalQuestions[j].chapterId,
                  numericalQuestions[j].questionCount - questionCount,
                ]
              )
              .then((rs) => {
                this.questionCount += rs.rows.length;
                for (let i = 0; i < rs.rows.length; i++) {
                  this.questions[j].questionCount += 1;
                  // this.questions[i].questionCount + 1;
                  this.questionsList.push({
                    QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                    QUESTION: rs.rows.item(i).QUESTION,
                    OPTIONA: rs.rows.item(i).OPTIONA,
                    OPTIONB: rs.rows.item(i).OPTIONB,
                    OPTIONC: rs.rows.item(i).OPTIONC,
                    OPTIOND: rs.rows.item(i).OPTIOND,
                    ANSWER: rs.rows.item(i).ANSWER,
                    HINT: rs.rows.item(i).HINT,
                    QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                    QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                    QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                    ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                    QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                    ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                    OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                    ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                    HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                    CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                    SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                    ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                    ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                    NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                    UNIT: rs.rows.item(i).UNIT,
                    userAns: "",
                    userNumericalAns: "",
                    userUnit: "",
                  });
                }
                // this.questionsList = this.questionsList.sort(
                //   (a, b) => a.QUESTION_ID - b.QUESTION_ID
                // );
                // if (j == this.questions.length - 1) this.showTest();
              });
          }
          if (j == numericalQuestions.length - 1) {
            // console.log("for loop completed");
            this.questionsList = this.questionsList.sort(
              (a, b) => a.QUESTION_ID - b.QUESTION_ID
            );
            this.questionCount = this.questionsList.length;
            console.log("questionList", this.questionsList);
            // console.log("que", this.questionsList.length);
            this.showTest();
            this.onReady();
          }
        });
    }
  }

  getQuestionsForManualEasy() {
    this.settings.questionCount = this.storageServiceService.questionCount;
    this.chaptersList = this.chaptersList.sort((a, b) => a - b);
    this.chaptersList.map((chapterId) => {
      this.questions.push({
        chapterId: chapterId,
        questionCount: 0,
      });
    });
    // console.log("questions", this.questions);
    // console.log("manual easy");
    if (this.cntManual.cntEasy > 0) {
      let easyQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntEasy / this.chaptersList.length
      );
      // console.log("question count", questionCount);
      let remainder = this.cntManual.cntEasy % this.chaptersList.length;
      // console.log("remainder", remainder);
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        easyQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        // console.log("in remainder");
        for (let i = 0; i < remainder; i++) {
          easyQuestions[i].questionCount++;
        }
      }
      // console.log("easy questions", easyQuestions);
      // easyQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      let retrievedQue: number = 0;
      for (let j = 0; j < easyQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        }  AND QUESTION_LEVEL=0 AND CHAPTER_ID = ? limit ?`,
            [easyQuestions[j].chapterId, easyQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            // console.log("quePerChapter", rs.rows.length);
            // this.questions.find(
            //   ({ chapterId }) => chapterId == easyQuestions[j].chapterId
            // ).questionCount =
            //   this.questions.find(
            //     ({ chapterId }) => chapterId == easyQuestions[j].chapterId
            //   ).questionCount + rs.rows.length;
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == easyQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntEasy - retrievedQue;
              if (retrievedQue < this.cntManual.cntEasy) {
                let stopLoop = false;
                for (let k = 0; k < easyQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  }  AND CHAPTER_ID = ? AND QUESTION_LEVEL=0 AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [easyQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntAtt +
                  this.cntManual.cntHard +
                  this.cntManual.cntMedium +
                  this.cntManual.cntNonAtt +
                  this.cntManual.cntNum +
                  this.cntManual.cntNumNew +
                  this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualMedium();
            }
          });
      }
    } else {
      this.getQuestionsForManualMedium();
    }
  }

  getQuestionsForManualMedium() {
    // console.log("manual med");
    if (this.cntManual.cntMedium > 0) {
      let mediumQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntMedium / this.chaptersList.length
      );
      let remainder = this.cntManual.cntMedium % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        mediumQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          mediumQuestions[i].questionCount++;
        }
      }
      // mediumQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      let retrievedQue: number = 0;
      for (let j = 0; j < mediumQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        }  AND QUESTION_LEVEL=1 AND CHAPTER_ID = ? limit ?`,
            [mediumQuestions[j].chapterId, mediumQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == mediumQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntMedium - retrievedQue;
              if (retrievedQue < this.cntManual.cntMedium) {
                let stopLoop = false;
                for (let k = 0; k < mediumQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID, ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND CHAPTER_ID = ? AND QUESTION_LEVEL=1 AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [mediumQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntAtt +
                  this.cntManual.cntHard +
                  this.cntManual.cntNonAtt +
                  this.cntManual.cntNum +
                  this.cntManual.cntNumNew +
                  this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualHard();
            }
          });
      }
    } else {
      this.getQuestionsForManualHard();
    }
  }

  getQuestionsForManualHard() {
    // console.log("manual hard");
    if (this.cntManual.cntHard > 0) {
      let hardQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntHard / this.chaptersList.length
      );
      let remainder = this.cntManual.cntHard % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        hardQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          hardQuestions[i].questionCount++;
        }
      }
      // hardQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      let retrievedQue: number = 0;
      for (let j = 0; j < hardQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_LEVEL=2 AND CHAPTER_ID = ? limit ?`,
            [hardQuestions[j].chapterId, hardQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == hardQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntHard - retrievedQue;
              if (retrievedQue < this.cntManual.cntHard) {
                let stopLoop = false;
                for (let k = 0; k < hardQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND CHAPTER_ID = ? AND QUESTION_LEVEL=2 AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [hardQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntAtt +
                  this.cntManual.cntNonAtt +
                  this.cntManual.cntNum +
                  this.cntManual.cntNumNew +
                  this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualAttempted();
            }
          });
      }
    } else {
      this.getQuestionsForManualAttempted();
    }
  }

  getQuestionsForManualAttempted() {
    // console.log("manual att");
    if (this.cntManual.cntAtt > 0) {
      let attQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntAtt / this.chaptersList.length
      );
      let remainder = this.cntManual.cntAtt % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        attQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          attQuestions[i].questionCount++;
        }
      }
      // attQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < attQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 1"
            : "ATTEMPTEDVALUE = 1"
        } AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [attQuestions[j].chapterId, attQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == attQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntAtt - retrievedQue;
              if (retrievedQue < this.cntManual.cntAtt) {
                let stopLoop = false;
                for (let k = 0; k < attQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 1"
                      : "ATTEMPTEDVALUE = 1"
                  } AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [attQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntNonAtt +
                  this.cntManual.cntNum +
                  this.cntManual.cntNumNew +
                  this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualNonAttempted();
            }
          });
      }
    } else {
      this.getQuestionsForManualNonAttempted();
    }
  }

  getQuestionsForManualNonAttempted() {
    // console.log("manual nonatt");
    if (this.cntManual.cntNonAtt > 0) {
      let nonAttQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntNonAtt / this.chaptersList.length
      );
      let remainder = this.cntManual.cntNonAtt % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        nonAttQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          nonAttQuestions[i].questionCount++;
        }
      }
      // nonAttQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < nonAttQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [nonAttQuestions[j].chapterId, nonAttQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == nonAttQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntNonAtt - retrievedQue;
              if (retrievedQue < this.cntManual.cntNonAtt) {
                let stopLoop = false;
                for (let k = 0; k < nonAttQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [nonAttQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntNum +
                  this.cntManual.cntNumNew +
                  this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualNumerical();
            }
          });
      }
    } else {
      this.getQuestionsForManualNumerical();
    }
  }

  getQuestionsForManualNumerical() {
    // console.log("manual numerical");
    if (this.cntManual.cntNum > 0) {
      let numericalQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntNum / this.chaptersList.length
      );
      let remainder = this.cntManual.cntNum % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        numericalQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          numericalQuestions[i].questionCount++;
        }
      }
      // numericalQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < numericalQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_TYPE=1 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [
              numericalQuestions[j].chapterId,
              numericalQuestions[j].questionCount,
            ]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == numericalQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntNum - retrievedQue;
              if (retrievedQue < this.cntManual.cntNum) {
                let stopLoop = false;
                for (let k = 0; k < numericalQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND QUESTION_TYPE=1 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [numericalQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory +
                  this.cntManual.cntNumNew ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualNumericalNew();
            }
          });
      }
    } else {
      this.getQuestionsForManualNumericalNew();
    }
  }

  getQuestionsForManualNumericalNew() {
    // console.log("manual numerical new");
    if (this.cntManual.cntNumNew > 0) {
      let numericalNewQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntNumNew / this.chaptersList.length
      );
      let remainder = this.cntManual.cntNumNew % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        numericalNewQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          numericalNewQuestions[i].questionCount++;
        }
      }
      // numericalQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < numericalNewQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_TYPE=2 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [
              numericalNewQuestions[j].chapterId,
              numericalNewQuestions[j].questionCount,
            ]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == numericalNewQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntNumNew - retrievedQue;
              if (retrievedQue < this.cntManual.cntNumNew) {
                let stopLoop = false;
                for (let k = 0; k < numericalNewQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND QUESTION_TYPE=2 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [numericalNewQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (
                this.cntManual.cntPrevAsk +
                  this.cntManual.cntTheory +
                  this.cntManual.cntNumNew ==
                0
              ) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualTheory();
            }
          });
      }
    } else {
      this.getQuestionsForManualTheory();
    }
  }

  getQuestionsForManualTheory() {
    // console.log("manual theory");
    if (this.cntManual.cntTheory > 0) {
      let theoryQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntTheory / this.chaptersList.length
      );
      let remainder = this.cntManual.cntTheory % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        theoryQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          theoryQuestions[i].questionCount++;
        }
      }
      // theoryQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < theoryQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_TYPE=0 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [theoryQuestions[j].chapterId, theoryQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == theoryQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntTheory - retrievedQue;
              if (retrievedQue < this.cntManual.cntTheory) {
                let stopLoop = false;
                for (let k = 0; k < theoryQuestions.length; k++) {
                  if (stopLoop) break;
                  // console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND QUESTION_TYPE=0 AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [theoryQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (this.cntManual.cntPrevAsk == 0) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
              this.getQuestionsForManualPrevAsk();
            }
          });
      }
    } else {
      this.getQuestionsForManualPrevAsk();
    }
  }

  getQuestionsForManualPrevAsk() {
    // console.log("manual prev");
    if (this.cntManual.cntPrevAsk > 0) {
      let prevAskQuestions: {
        chapterId: number;
        questionCount: number;
      }[] = [];
      let questionCount = Math.floor(
        this.cntManual.cntPrevAsk / this.chaptersList.length
      );
      let remainder = this.cntManual.cntPrevAsk % this.chaptersList.length;
      // console.log("chapterList", this.chaptersList);
      this.chaptersList.map((chapter) => {
        prevAskQuestions.push({
          chapterId: chapter,
          questionCount: questionCount,
        });
      });
      if (remainder > 0) {
        for (let i = 0; i < remainder; i++) {
          prevAskQuestions[i].questionCount++;
        }
      }
      // prevAskQuestions.map((question) => {
      //   if (
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     )
      //   ) {
      //     this.questions.find(
      //       ({ chapterId }) => chapterId == question.chapterId
      //     ).questionCount =
      //       this.questions.find(
      //         ({ chapterId }) => chapterId == question.chapterId
      //       ).questionCount + question.questionCount;
      //   } else {
      //     this.questions.push(question);
      //   }
      // });
      // console.log("questions", this.questions);
      let retrievedQue: number = 0;
      for (let j = 0; j < prevAskQuestions.length; j++) {
        // console.log("j", j);
        this.databaseServiceService
          .getDataBase()
          .executeSql(
            `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, 
        CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM 
        question_info WHERE ${
          this.loggedInType == "demo"
            ? "ATTEMPTED_DEMO = 0"
            : "ATTEMPTEDVALUE = 0"
        } AND QUESTION_YEAR!= '' AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()}) limit ?`,
            [prevAskQuestions[j].chapterId, prevAskQuestions[j].questionCount]
          )
          .then((rs) => {
            for (let i = 0; i < rs.rows.length; i++) {
              this.questionsList.push({
                QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                QUESTION: rs.rows.item(i).QUESTION,
                OPTIONA: rs.rows.item(i).OPTIONA,
                OPTIONB: rs.rows.item(i).OPTIONB,
                OPTIONC: rs.rows.item(i).OPTIONC,
                OPTIOND: rs.rows.item(i).OPTIOND,
                ANSWER: rs.rows.item(i).ANSWER,
                HINT: rs.rows.item(i).HINT,
                QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                UNIT: rs.rows.item(i).UNIT,
                userAns: "",
                userNumericalAns: "",
                userUnit: "",
              });
              retrievedQue++;
            }
            this.questions[j].questionCount =
              this.questions[j].questionCount + rs.rows.length;
            if (j == prevAskQuestions.length - 1) {
              this.questionsList.map((questions) => {
                this.existingQuestions.push(questions.QUESTION_ID);
              });
              let remainingQue = this.cntManual.cntPrevAsk - retrievedQue;
              if (retrievedQue < this.cntManual.cntPrevAsk) {
                let stopLoop = false;
                for (let k = 0; k < prevAskQuestions.length; k++) {
                  if (stopLoop) break;
                  console.log("remaining que", remainingQue);
                  this.databaseServiceService
                    .getDataBase()
                    .executeSql(
                      `SELECT QUESTION_ID, QUESTION, OPTIONA, OPTIONB, OPTIONC,
                  OPTIOND, ANSWER, HINT, QUESTION_LEVEL, SUBJECT_ID, CHAPTER_ID,ATTEMPTEDVALUE, ATTEMPTED_DEMO, QUESTION_TYPE, QUESTION_YEAR, 
                  ISQUESTIONASIMAGE,QUESTIONIMAGEPATH,ISOPTIONASIMAGE,OPTIONIMAGEPATH,ISHINTASIMAGE,HINTIMAGEPATH, NUMERICAL_ANSWER, UNIT FROM question_info 
                  WHERE ${
                    this.loggedInType == "demo"
                      ? "ATTEMPTED_DEMO = 0"
                      : "ATTEMPTEDVALUE = 0"
                  } AND QUESTION_YEAR!= '' AND CHAPTER_ID = ? AND QUESTION_ID NOT IN (${this.existingQuestions.toString()})`,
                      [prevAskQuestions[k].chapterId]
                    )
                    .then((rs) => {
                      if (stopLoop) return;
                      for (let i = 0; i < rs.rows.length; i++) {
                        remainingQue--;
                        this.questions[k].questionCount =
                          this.questions[k].questionCount + 1;
                        this.questionsList.push({
                          QUESTION_ID: rs.rows.item(i).QUESTION_ID,
                          QUESTION: rs.rows.item(i).QUESTION,
                          OPTIONA: rs.rows.item(i).OPTIONA,
                          OPTIONB: rs.rows.item(i).OPTIONB,
                          OPTIONC: rs.rows.item(i).OPTIONC,
                          OPTIOND: rs.rows.item(i).OPTIOND,
                          ANSWER: rs.rows.item(i).ANSWER,
                          HINT: rs.rows.item(i).HINT,
                          QUESTION_YEAR: rs.rows.item(i).QUESTION_YEAR,
                          QUESTION_LEVEL: rs.rows.item(i).QUESTION_LEVEL,
                          QUESTION_TYPE: rs.rows.item(i).QUESTION_TYPE,
                          ISQUESTIONASIMAGE: rs.rows.item(i).ISQUESTIONASIMAGE,
                          QUESTIONIMAGEPATH: rs.rows.item(i).QUESTIONIMAGEPATH,
                          ISOPTIONASIMAGE: rs.rows.item(i).ISOPTIONASIMAGE,
                          OPTIONIMAGEPATH: rs.rows.item(i).OPTIONIMAGEPATH,
                          ISHINTASIMAGE: rs.rows.item(i).ISHINTASIMAGE,
                          HINTIMAGEPATH: rs.rows.item(i).HINTIMAGEPATH,
                          CHAPTER_ID: rs.rows.item(i).CHAPTER_ID,
                          SUBJECT_ID: rs.rows.item(i).SUBJECT_ID,
                          ATTEMPTED_VALUE: rs.rows.item(i).ATTEMPTEDVALUE,
                          ATTEMPTED_DEMO: rs.rows.item(i).ATTEMPTED_DEMO,
                          NUMERICAL_ANSWER: rs.rows.item(i).NUMERICAL_ANSWER,
                          UNIT: rs.rows.item(i).UNIT,
                          userAns: "",
                          userNumericalAns: "",
                          userUnit: "",
                        });
                        this.questionsList = this.questionsList.sort(
                          (a, b) => a.QUESTION_ID - b.QUESTION_ID
                        );
                        if (remainingQue == 0) {
                          stopLoop = true;
                          // console.log("in break");
                          break;
                        }
                      }
                    });
                }
              }
              this.questionsList = this.questionsList.sort(
                (a, b) => a.QUESTION_ID - b.QUESTION_ID
              );
              if (this.cntManual.cntPrevAsk != 0) {
                this.showTest();
                this.onReady();
              }
              // console.log("questionList sort", this.questionsList);
              this.questionCount = this.questionsList.length + remainingQue;
              // console.log("questionList", this.questionsList);
              // console.log("que", this.questionsList.length + remainingQue);
            }
          });
      }
    }
  }

  // getChapterWiseQueCount() {
  //   let questions = [];
  //   this.chaptersList.map(chapterNo => {
  //     questions = this.questionsList.filter(({CHAPTER_ID}) => CHAPTER_ID == chapterNo);
  //     this.chapterWiseQueCount.push({
  //       chapterNo: chapterNo,
  //       queCount: questions.length
  //     })
  //   })
  //   console.log('chapterwise que count', this.chapterWiseQueCount);
  // }

  onBackClick() {
    if (
      this.page == "unit-test" ||
      this.page == "saved-test" ||
      this.page == "paused-test" ||
      this.page == "question-papers"
    ) {
      if (this.testSubmitted == true) {
        this.testView = false;
        // console.log("test view", this.testView);
      } else {
        clearTimeout(this.stopClockInterval);
        this.router.navigateByUrl("/dashboard/unit-test");
      }
    } else if (this.page == "bookmark") {
      this.navController.back();
    }
  }

  next() {
    if (this.index < this.questionCount - 1) {
      this.index = this.index + 1;
      this.showTest();
      if (this.questionsList[this.index].QUESTION_TYPE != 2) {
        if (
          (this.page == "unit-test" ||
            this.page == "paused-test" ||
            this.page == "question-papers") &&
          this.testSubmitted == false
        ) {
          this.funSelectedOption();
        } else {
          this.inCorrOption = "";
          this.option = this.questionsList[this.index].ANSWER;
          if (
            this.questionsList[this.index].ANSWER !=
            this.questionsList[this.index].userAns
          ) {
            this.inCorrOption = this.questionsList[this.index].userAns;
          }
          if (
            (this.page == "saved-test" ||
              this.page == "unit-test" ||
              this.page == "paused-test" ||
              this.page == "question-papers") &&
            this.testSubmitted == true
          ) {
            this.getQuestionStatus();
          }
        }
      } else {
        this.selectionCleared = false;
        this.answer = this.questionsList[this.index].userNumericalAns;
        this.unit = this.questionsList[this.index].userUnit;
        if (
          (this.page == "saved-test" ||
            this.page == "unit-test" ||
            this.page == "paused-test") &&
          this.testSubmitted == true
        ) {
          this.getQuestionStatus();
        }
      }
    } else {
      this.createAlert("You are on the last question");
    }
  }

  previous() {
    if (this.index > 0) {
      this.index = this.index - 1;
      this.showTest();
      if (this.questionsList[this.index].QUESTION_TYPE != 2) {
        if (
          (this.page == "unit-test" ||
            this.page == "paused-test" ||
            this.page == "question-papers") &&
          this.testSubmitted == false
        ) {
          this.funSelectedOption();
        } else {
          this.inCorrOption = "";
          this.option = this.questionsList[this.index].ANSWER;
          if (
            this.questionsList[this.index].ANSWER !=
            this.questionsList[this.index].userAns
          ) {
            this.inCorrOption = this.questionsList[this.index].userAns;
          }
          if (
            (this.page == "saved-test" ||
              this.page == "unit-test" ||
              this.page == "paused-test" ||
              this.page == "question-papers") &&
            this.testSubmitted == true
          ) {
            this.getQuestionStatus();
          }
        }
      } else {
        this.selectionCleared = false;
        this.answer = this.questionsList[this.index].userNumericalAns;
        this.unit = this.questionsList[this.index].userUnit;
        if (
          (this.page == "saved-test" ||
            this.page == "unit-test" ||
            this.page == "paused-test") &&
          this.testSubmitted == true
        ) {
          this.getQuestionStatus();
        }
      }
    } else {
      this.createAlert("You are on the first question");
    }
  }

  showTest() {
    // console.log("in show test");
    this.iconCount = this.questionsList[this.index].QUESTION_LEVEL;
    // if(this.bookmarkQustions.includes(this.questionsList[this.index].QUESTION_ID)) {
    //   console.log('in show test if');
    //   this.bookmarkIcon = 'bookmark'
    // } else {
    //   this.bookmarkIcon = 'bookmark-outline'
    //   console.log('in show test else');
    // }

    if (
      this.bookmarkQustions.find(
        ({ questionId }) =>
          questionId == this.questionsList[this.index].QUESTION_ID
      ) == null
    ) {
      this.bookmarkIcon = "bookmark-outline";
      // console.log("in show test if");
    } else {
      this.bookmarkIcon = "bookmark";
      // console.log("in show test else");
    }
    document.getElementById("question").innerHTML =
      "$" + this.questionsList[this.index].QUESTION + "$";
    document.getElementById("opt1").innerHTML =
      "$" + this.questionsList[this.index].OPTIONA + "$";
    document.getElementById("opt2").innerHTML =
      "$" + this.questionsList[this.index].OPTIONB + "$";
    document.getElementById("opt3").innerHTML =
      "$" + this.questionsList[this.index].OPTIONC + "$";
    document.getElementById("opt4").innerHTML =
      "$" + this.questionsList[this.index].OPTIOND + "$";
    eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    // if(this.questionsList[this.index].QUESTION_YEAR != '') {
    //   document.getElementById('prevAsk').innerHTML = this.questionsList[this.index].QUESTION_YEAR;
    // }
    if (this.page == "bookmark") {
      document.getElementById("hint").innerHTML =
        "$" + this.questionsList[this.index].HINT + "$";
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
      this.option = this.questionsList[this.index].ANSWER;
      this.answer = this.questionsList[this.index].NUMERICAL_ANSWER;
      this.unit = this.questionsList[this.index].UNIT;
    }
    if (this.testSubmitted == true) {
      document.getElementById("hint").innerHTML =
        "$" + this.questionsList[this.index].HINT + "$";
      eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
    }
    this.questionsList[this.index].QUESTION_TYPE == 2
      ? (this.showInputBox = true)
      : (this.showInputBox = false);
    if (this.questionsList[this.index].ISQUESTIONASIMAGE == 1) {
      this.questionImage = false;
      this.questionImagePath = `assets/${
        this.questionsList[this.index].QUESTIONIMAGEPATH
      }`;
    } else {
      this.questionImage = true;
      this.questionImagePath = "";
    }

    if (this.questionsList[this.index].ISOPTIONASIMAGE == 1) {
      this.optionImage = false;
      this.optionImagePath = `assets/${
        this.questionsList[this.index].OPTIONIMAGEPATH
      }`;
    } else {
      this.optionImage = true;
      this.optionImagePath = "";
    }

    if (
      this.testSubmitted == true &&
      this.questionsList[this.index].ISHINTASIMAGE == 1
    ) {
      this.hintImage = false;
      this.hintImagePath = `assets/${
        this.questionsList[this.index].HINTIMAGEPATH
      }`;
    } else {
      this.hintImage = true;
      this.hintImagePath = "";
    }
  }

  funSelectedOption() {
    // this.hintShown = false;
    this.option = "";
    // document.getElementById('hint').innerHTML = ''
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QUESTION_ID
    );
    // console.log("selectedOptions", this.selectedOption);
    if (this.selectedOption == null) {
      this.option = "";
      this.selectionCleared = false;
    } else {
      this.option = this.selectedOption.option;
      this.selectionCleared = true;
    }
    // this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option;
    // console.log(this.selectedOptionArr.find(({questionNo}) => questionNo == this.index).option);
    // console.log(this.questionsList[this.index].ANSWER);
  }

  onReady() {
    if (this.page == "paused-test")
      this.secs =
        parseInt(this.testDetails.timeTakenMin) * 60 +
        parseInt(this.testDetails.timeTakenSec);
    this.stopClockInterval = setInterval((_) => this.countTime(), 1000);
  }

  async countTime() {
    this.secs--;
    this.fmins = Math.floor(this.secs / 60);
    this.fsecs = Math.floor(this.secs % 60);
    if (this.fmins <= 9) {
      this.fmins = "0" + this.fmins;
    }
    if (this.fsecs <= 9) {
      this.fsecs = "0" + this.fsecs;
    }
    if (this.secs >= 0) {
      document.getElementById("timer").innerHTML =
        this.fmins + ":" + this.fsecs;
    } else {
      clearTimeout(this.stopClockInterval);
      const alert = await this.alertController.create({
        subHeader: "Your test time exceeds",
        cssClass: "alert-title",
        animated: true,
        buttons: [
          {
            text: "Ok",
            handler: () => {
              this.popoverController.dismiss();
              this.submit();
            },
          },
        ],
        backdropDismiss: false,
      });
      await alert.present();
    }
  }

  clickOpt1() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("A");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt2() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("B");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt3() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("C");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.createToast("you cannot select another option", 1000);
      }
    }
  }

  clickOpt4() {
    if (this.testSubmitted == false) {
      if (
        (this.page == "unit-test" ||
          this.page == "paused-test" ||
          this.page == "question-papers") &&
        this.selectionCleared == false
      ) {
        this.selectOption("D");
      } else if (
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers"
      ) {
        this.createToast("you cannot select another option", 1000);
      }
    }
  }

  selectOption(option: string) {
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QUESTION_ID
    );
    // console.log("selected in opt1", this.selectedOption);
    if (this.selectedOptionArr.includes(this.selectedOption)) {
      // console.log("in opt4 if");
      this.selectedOptionArr.splice(
        this.selectedOptionArr.indexOf(this.selectedOption),
        1
      );
      this.questionsList[this.index].userAns = "";
    }
    this.selectedOption = {
      questionNo: this.questionsList[this.index].QUESTION_ID,
      option: option,
      userNumericalAns: "",
      userUnit: "",
    };
    this.option = option;
    this.questionsList[this.index].userAns = option;
    // console.log("user ans", this.questionsList[this.index].userAns);
    this.selectedOptionArr.push(this.selectedOption);
    this.selectionCleared = true;
    // console.log("selectedArr", this.selectedOptionArr);
  }

  // enteredAnswer(answer, unit) {
  //   this.selectedOption = this.selectedOptionArr.find(
  //     ({ questionNo }) =>
  //       questionNo === this.questionsList[this.index].QUESTION_ID
  //   );
  // }

  onAnswerChange() {
    this.questionsList[this.index].userNumericalAns = this.answer;
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QUESTION_ID
    );
    if (this.answer.length == 0) {
      this.unit = "";
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr.splice(
          this.selectedOptionArr.indexOf(this.selectedOption),
          1
        );
      }
    } else {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userNumericalAns = this.answer;
      } else {
        this.selectedOptionArr.push({
          questionNo: this.questionsList[this.index].QUESTION_ID,
          option: "",
          userNumericalAns: this.answer,
          userUnit: this.unit,
        });
      }
    }
  }

  onUnitChange() {
    this.questionsList[this.index].userUnit = this.unit;
    this.selectedOption = this.selectedOptionArr.find(
      ({ questionNo }) =>
        questionNo === this.questionsList[this.index].QUESTION_ID
    );
    if (this.unit.length == 0 && this.answer.length != 0) {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userUnit = "";
      }
    } else if (this.unit.length > 0 && this.answer.length > 0) {
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        this.selectedOptionArr[
          this.selectedOptionArr.indexOf(this.selectedOption)
        ].userUnit = this.unit;
      }
    }
  }

  // showHint() {
  //   this.hintShown = !this.hintShown;
  //   console.log('hint', this.hintShown);
  //   if (this.hintShown == true) {
  //     document.getElementById('hint').innerHTML = '$' + this.questionsList[this.index].HINT + '$';
  //     eval('MathJax.Hub.Queue(["Typeset", MathJax.Hub])');
  //   } else {
  //     document.getElementById('hint').innerHTML = '';
  //   }
  // }

  async go() {
    this.inGo = true;
    if (this.count == 0) {
      this.totalQuestions = [];
      let index = 0;
      this.questionsList.map((questions) => {
        this.totalQuestions.push({
          index: index,
          questionNo: questions.QUESTION_ID,
          questionType: questions.QUESTION_TYPE,
        });
        index++;
      });
      this.count++;
    }
    const modal = await this.popoverController.create({
      component: QuestionPanelPage,
      backdropDismiss: false,
      animated: true,
      cssClass: "myPopOver3",
      componentProps: {
        subjectName: this.subjectName,
        totalQuestions: this.totalQuestions,
        answeredQuestions: this.selectedOptionArr,
        phyQuestionsStart: this.phyQuestionStarts,
        chemQuestionsStart: this.chemQuestionStarts,
        mathsQuestionsStart: this.mathsQuestionStarts,
        index: this.index,
      },
    });
    await modal.present();
    modal.onDidDismiss().then((_) => {
      this.inGo = false;
      this.storage.ready().then((ready) => {
        if (ready) {
          this.storage.get("index").then((index) => {
            // console.log("index in modal", index);
            if (index == null) {
            } else this.index = index;
            this.showTest();
            this.storage.remove("index");
            if (this.questionsList[this.index].QUESTION_TYPE != 2) {
              if (
                this.page == "unit-test" ||
                this.page == "paused-test" ||
                this.page == "question-papers"
              ) {
                this.funSelectedOption();
              }
            } else {
              this.selectionCleared = false;
              this.answer = this.questionsList[this.index].userNumericalAns;
              this.unit = this.questionsList[this.index].userUnit;
            }
          });
        }
      });
    });
  }

  bookmark() {
    // this.questionBookmarkService.selectId(this.questionsList[this.index].QUESTION_ID).then(result => {
    //   if (result) {
    //     this.createAlert('Question Already Bookmarked');
    //   } else {
    //     this.bookmarkQuestion();
    //   }
    // })
    // if(this.bookmarkQustions.includes(this.questionsList[this.index].QUESTION_ID)) {
    //   console.log('in book if');
    //   this.bookmarkQustions.splice(this.bookmarkQustions.indexOf(this.questionsList[this.index].QUESTION_ID), 1);
    //   this.removedBookmarkQustions.push(this.questionsList[this.index].QUESTION_ID);
    //   this.createToast('Question Removed from Bookmark', 1000);
    //   this.bookmarkIcon = 'bookmark-outline';
    //   console.log('book',this.bookmarkQustions);
    //   console.log('removed',this.removedBookmarkQustions);
    // } else {
    //   console.log('in book else');
    //   this.bookmarkQustions.push(this.questionsList[this.index].QUESTION_ID);
    //   if(this.removedBookmarkQustions.includes(this.questionsList[this.index].QUESTION_ID)) {
    //     this.removedBookmarkQustions.splice(this.questionsList.indexOf(this.questionsList[this.index].QUESTION_ID), 1);
    //   }
    //   this.createToast('Question Added to Bookmark', 1000);
    //   this.bookmarkIcon = 'bookmark';
    //   console.log('book',this.bookmarkQustions);
    //   console.log('removed',this.removedBookmarkQustions);
    // }

    let bookmarkedQuestion = this.bookmarkQustions.find(
      ({ questionId }) =>
        questionId == this.questionsList[this.index].QUESTION_ID
    );
    // console.log("bookmarked", bookmarkedQuestion);
    if (bookmarkedQuestion == null) {
      this.bookmarkQustions.push({
        questionId: this.questionsList[this.index].QUESTION_ID,
        chapterId: this.questionsList[this.index].CHAPTER_ID,
      });
      if (
        this.removedBookmarkQustions.find(
          ({ questionId }) =>
            questionId == this.questionsList[this.index].QUESTION_ID
        ) == null
      ) {
        // console.log("in remove if");
      } else {
        // console.log("in remove else");
        this.removedBookmarkQustions.splice(
          this.removedBookmarkQustions.indexOf(
            this.removedBookmarkQustions.find(
              ({ questionId }) =>
                questionId == this.questionsList[this.index].QUESTION_ID
            )
          ),
          1
        );
      }
      // console.log("book", this.bookmarkQustions);
      // console.log("removed", this.removedBookmarkQustions);
      this.createToast("Question Added to Bookmark", 1000);
      this.bookmarkIcon = "bookmark";
    } else {
      this.bookmarkQustions.splice(
        this.bookmarkQustions.indexOf(bookmarkedQuestion),
        1
      );
      this.removedBookmarkQustions.push({
        questionId: this.questionsList[this.index].QUESTION_ID,
        chapterId: this.questionsList[this.index].CHAPTER_ID,
      });
      this.createToast("Question Removed from Bookmark", 1000);
      this.bookmarkIcon = "bookmark-outline";
      // console.log("book", this.bookmarkQustions);
      // console.log("removed", this.removedBookmarkQustions);
    }
  }

  // async createToast() {
  //   let toast = await this.toastController.create({
  //     message: 'Question Bookmark Successfully',
  //     animated: true,
  //     position: 'top',
  //     duration: 2000,
  //   })
  // await toast.present();
  // const dismiss = await toast.onDidDismiss();
  // if (dismiss.role == 'cancel') {
  //   console.log('toast canceled');
  // } else {
  //   console.log('questionId', this.questionsList[this.index].QUESTION_ID);
  //   console.log('chapterId', this.questionsList[this.index].CHAPTER_ID);
  //   this.questionBookmarkService.insertBookmarkQue(this.questionsList[this.index].QUESTION_ID, this.questionsList[this.index].CHAPTER_ID);
  // }
  // }

  async createAlert(message) {
    let alert = await this.alertController.create({
      subHeader: message,
      cssClass: "alert-title",
      animated: true,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
        },
      ],
    });
    await alert.present();
  }

  async onSubmit() {
    const alert = await this.alertController.create({
      subHeader: "Do you want to submit test ?",
      cssClass: "alert-title",
      animated: true,
      backdropDismiss: false,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Confirm",
          handler: () => {
            clearTimeout(this.stopClockInterval);
            this.submit();
          },
        },
      ],
    });
    await alert.present();
  }

  submit() {
    this.testView = false;
    this.testSubmitted = true;
    // this.questionsList.map((question) => {
    //   if (
    //     question.QUESTION_TYPE == 2 &&
    //     question.userNumericalAns != ""
    //   ) {
    //     this.selectedOptionArr.push({
    //       questionNo: question.QUESTION_ID,
    //       option: "",
    //       userNumericalAns: question.userNumericalAns,
    //       userUnit: question.userUnit,
    //     });
    //   }
    // });
    // console.log("after submit", this.selectedOptionArr);
    this.outOfMarks = this.questionsList.length * this.settings.correctMarks;
    this.corrMarks = this.settings.correctMarks;
    this.inCorrMarks = this.settings.inCorrectMarks;
    this.setAttemptedValue();
    // if (this.page == 'unit-test') {
    //   clearTimeout(this.stopClockInterval);
    //   this.router.navigateByUrl('/dashboard/unit-test');
    // } else if (this.page == 'bookmark') {
    //   this.router.navigateByUrl('/dashboard/bookmark');
    // }
    this.getMarks();
    if (this.subjectName.startsWith("JEE")) {
      this.getPhyMarks();
      this.getChemMarks();
      this.getMathsMarks();
    }
    this.getTime();
    if (this.page == "unit-test" || this.page == "question-papers")
      this.getTestId();
    else if (this.page == "paused-test") this.saveUserAns();
    this.saveTest();
  }

  setAttemptedValue() {
    if (this.page != "question-papers") {
      // console.log("updated attempted", this.selectedOptionArr);
      let questionId = [];
      this.selectedOptionArr.map((selectedOption) => {
        questionId.push(selectedOption.questionNo);
      });
      this.databaseServiceService
        .getDataBase()
        .executeSql(
          `UPDATE question_info SET ${
            this.loggedInType == "demo"
              ? "ATTEMPTED_DEMO = 1"
              : "ATTEMPTEDVALUE = 1"
          } WHERE QUESTION_ID IN (${questionId.toString()})`,
          []
        )
        .then((data) => {
          // console.log("data in setAttemptedValue", data);
        });
    }
  }

  onResult() {
    this.testView = false;
  }

  getMarks() {
    let numericalCorrectQuestions: number = 0;
    for (let i = 0; i < this.questionsList.length; i++) {
      let questionNo = this.selectedOptionArr.find(
        ({ questionNo }) => questionNo == this.questionsList[i].QUESTION_ID
      );
      if (questionNo != null) {
        if (questionNo.option != "") {
          if (questionNo.option == this.questionsList[i].ANSWER) {
            this.totalMarks = this.totalMarks + this.corrMarks;
            this.correctQuestions++;
          } else {
            this.totalMarks = this.totalMarks - this.inCorrMarks;
            this.inCorrectQuestions++;
          }
        } else if (questionNo.userNumericalAns != "") {
          if (
            questionNo.userNumericalAns ==
              this.questionsList[i].NUMERICAL_ANSWER &&
            questionNo.userUnit == this.questionsList[i].UNIT
          ) {
            this.correctQuestions++;
            if (numericalCorrectQuestions < 5) {
              numericalCorrectQuestions += 1;
              console.log('numericalCorrectQuestions', numericalCorrectQuestions)
              this.totalMarks = this.totalMarks + this.corrMarks;
            }
          } else {
            // this.totalMarks = this.totalMarks - this.inCorrMarks;
            this.inCorrectQuestions++;
          }
        }
      }
    }
    this.skippedQuestions =
      this.questionsList.length -
      (this.correctQuestions + this.inCorrectQuestions);
    // console.log("skipped", this.skippedQuestions);
    // console.log("correct", this.correctQuestions);
    // console.log("incorrect", this.inCorrectQuestions);
    // console.log("total marks", this.totalMarks);
    this.createPieCharts();
    if (
      (this.page == "unit-test" || this.page == "paused-test") &&
      !this.subjectName.startsWith("JEE")
    )
      this.getSubjectWiseQuestionCount();
  }

  getPhyMarks() {
    for (let i = 0; i < this.chemQuestionStarts; i++) {
      let questionNo = this.selectedOptionArr.find(
        ({ questionNo }) => questionNo == this.questionsList[i].QUESTION_ID
      );
      if (questionNo != null) {
        if (questionNo.option == this.questionsList[i].ANSWER) {
          this.totalPhyMarks = this.totalPhyMarks + this.corrMarks;
        } else {
          this.totalPhyMarks = this.totalPhyMarks - this.inCorrMarks;
        }
      }
    }
    // console.log("phy marks", this.totalPhyMarks);
  }

  getChemMarks() {
    for (let i = this.chemQuestionStarts; i < this.mathsQuestionStarts; i++) {
      let questionNo = this.selectedOptionArr.find(
        ({ questionNo }) => questionNo == this.questionsList[i].QUESTION_ID
      );
      if (questionNo != null) {
        if (questionNo.option == this.questionsList[i].ANSWER) {
          this.totalChemMarks = this.totalChemMarks + this.corrMarks;
        } else {
          this.totalChemMarks = this.totalChemMarks - this.inCorrMarks;
        }
      }
    }
    // console.log("chem marks", this.totalChemMarks);
  }

  getMathsMarks() {
    for (let i = this.mathsQuestionStarts; i < this.questionsList.length; i++) {
      let questionNo = this.selectedOptionArr.find(
        ({ questionNo }) => questionNo == this.questionsList[i].QUESTION_ID
      );
      if (questionNo != null) {
        if (questionNo.option == this.questionsList[i].ANSWER) {
          this.totalMathsMarks = this.totalMathsMarks + this.corrMarks;
        } else {
          this.totalMathsMarks = this.totalMathsMarks - this.inCorrMarks;
        }
      }
    }
    // console.log("maths marks", this.totalMathsMarks);
  }

  getTime() {
    if (this.secs < 0) {
      this.totalMins =
        this.page == "question-papers" ? this.testTime : this.settings.time;
      this.totalSecs = "00";
    } else {
      let min =
        this.page == "question-papers"
          ? this.testTime - 1
          : this.settings.time - 1;
      let sec = 60;
      this.totalMins = min - this.fmins;
      this.totalSecs = sec - this.fsecs;
      if (this.totalSecs == 60) {
        this.totalMins = this.totalMins + 1;
        this.totalSecs = 0;
      }
      if (this.totalMins < 10) {
        this.totalMins = "0" + this.totalMins;
      }

      if (this.totalSecs < 10) {
        this.totalSecs = "0" + this.totalSecs;
      }
    }

    // console.log("total min", this.totalMins);
    // console.log("total secs", this.totalSecs);
  }

  createPieCharts() {
    if (this.page == "saved-test") {
      this.correctPer = Math.round(
        (this.testDetails.corrQueCount / this.testDetails.totalQue) * 100
      );
      this.inCorrectPer = Math.round(
        (this.testDetails.inCorrQueCount / this.testDetails.totalQue) * 100
      );
      this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
    } else if (
      this.page == "unit-test" ||
      this.page == "paused-test" ||
      this.page == "question-papers"
    ) {
      this.correctPer = Math.round(
        (this.correctQuestions / this.questionsList.length) * 100
      );
      this.inCorrectPer = Math.round(
        (this.inCorrectQuestions / this.questionsList.length) * 100
      );
      this.skippedPer = 100 - (this.correctPer + this.inCorrectPer);
    }

    this.pie1 = new Chart(this.pieChart1.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.correctPer, this.inCorrectPer, this.skippedPer],
            backgroundColor: ["#2dd36f", "#eb445a", "#3880ff"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });

    if (
      this.page == "unit-test" ||
      this.page == "paused-test" ||
      this.page == "question-papers"
    ) {
      if (this.selectedOptionArr.length == 0) {
        this.accuracy = 0;
      } else {
        this.accuracy = Math.round(
          (this.correctQuestions /
            (this.correctQuestions + this.inCorrectQuestions)) *
            100
        );
      }
    } else if (this.page == "saved-test") {
      if (this.testDetails.attQueCount == 0) {
        this.accuracy = 0;
      } else {
        this.accuracy = Math.round(
          (this.testDetails.corrQueCount /
            (this.testDetails.corrQueCount + this.testDetails.inCorrQueCount)) *
            100
        );
      }
    }

    this.pie2 = new Chart(this.pieChart2.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.accuracy, 100 - this.accuracy],
            backgroundColor: ["#2dd36f", "#eb445a"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });

    if (
      this.page == "unit-test" ||
      this.page == "paused-test" ||
      this.page == "question-papers"
    ) {
      this.score = Math.round(
        (this.totalMarks / (this.questionsList.length * this.corrMarks)) * 100
      );
    } else if (this.page == "saved-test") {
      this.score = Math.round(
        (this.testDetails.obtainedMarks / this.testDetails.totalMarks) * 100
      );
    }

    this.pie3 = new Chart(this.pieChart3.nativeElement, {
      type: "pie",
      data: {
        datasets: [
          {
            data: [this.score, 100 - this.score],
            backgroundColor: ["#2dd36f", "#eb445a"],
          },
        ],
      },
      options: {
        tooltips: {
          enabled: false,
        },
      },
    });

    // this.columnChart1 = {
    //   chartType: 'PieChart',
    //   dataTable: [
    //     ['City', '2010 Population'],
    //     ['New York City, NY', 8175000],
    //     ['Los Angeles, CA', 3792000],
    //     ['Chicago, IL', 2695000],
    //     ['Houston, TX', 2099000],
    //     ['Philadelphia, PA', 1526000]
    //   ],
    //   options: {
    //     'title': 'Tasks',
    //     height: 600,
    //     width: '100%',
    //     is3D: true
    //   },
    // };
  }

  getSubjectWiseQuestionCount() {
    let question;
    if (this.loggedInType == "demo") {
      this.selectedOptionArr.map((selectedOption) => {
        question = this.questionsList.find(
          ({ QUESTION_ID }) => QUESTION_ID === selectedOption.questionNo
        );
        if (question.SUBJECT_ID == 1 && question.ATTEMPTED_DEMO == 0) {
          this.overAllSummary.phyAttQue = this.overAllSummary.phyAttQue + 1;
        } else if (question.SUBJECT_ID == 2 && question.ATTEMPTED_DEMO == 0) {
          this.overAllSummary.chemAttQue = this.overAllSummary.chemAttQue + 1;
        } else if (question.SUBJECT_ID == 3 && question.ATTEMPTED_DEMO == 0) {
          this.overAllSummary.mathsAttQue = this.overAllSummary.mathsAttQue + 1;
        }
      });
    } else {
      this.selectedOptionArr.map((selectedOption) => {
        question = this.questionsList.find(
          ({ QUESTION_ID }) => QUESTION_ID === selectedOption.questionNo
        );
        if (question.SUBJECT_ID == 1 && question.ATTEMPTED_VALUE == 0) {
          this.overAllSummary.phyAttQue = this.overAllSummary.phyAttQue + 1;
        } else if (question.SUBJECT_ID == 2 && question.ATTEMPTED_VALUE == 0) {
          this.overAllSummary.chemAttQue = this.overAllSummary.chemAttQue + 1;
        } else if (question.SUBJECT_ID == 3 && question.ATTEMPTED_VALUE == 0) {
          this.overAllSummary.mathsAttQue = this.overAllSummary.mathsAttQue + 1;
        }
      });
    }
    this.updateOverAllSummary(this.overAllSummary);
  }

  updateOverAllSummary(overAllSummary: OverAllSummary) {
    this.overAllSummary.totalAttQue =
      this.correctQuestions +
      this.inCorrectQuestions +
      this.overAllSummary.totalAttQue;
    this.overAllSummary.totalCorrQue =
      this.correctQuestions + this.overAllSummary.totalCorrQue;
    this.overallSummaryService.updateOverAllSummary(
      overAllSummary,
      this.loggedInType == "demo" ? "overall_summary_demo" : "overall_summary"
    );
  }

  getTestId() {
    this.saveTestInfoService
      .getTestId(
        this.loggedInType == "demo" ? "save_test_info_demo" : "save_test_info"
      )
      .then((data) => {
        this.testId = data + 1;
        this.saveUserAns();
      });
  }

  saveTest() {
    // console.log("logged in type in save test", this.loggedInType);
    let day = new Date().getDate().toString();
    if (day.length == 1) day = "0" + day;
    let month = (new Date().getMonth() + 1).toString();
    if (month.length == 1) month = "0" + month;
    let year = new Date().getFullYear().toString();
    let submitDate = day + month + year;
    this.testDetails = {
      testDate: submitDate,
      subjectName: this.subjectName,
      totalQue: this.questionsList.length,
      corrMarks: this.settings.correctMarks,
      inCorrMarks: this.settings.inCorrectMarks,
      obtainedMarks: this.totalMarks,
      phyObtainedMarks: this.totalPhyMarks,
      chemObtainedMarks: this.totalChemMarks,
      mathsObtainedMarks: this.totalMathsMarks,
      totalMarks: this.outOfMarks,
      totalTime: this.settings.time,
      attQueCount: this.correctQuestions + this.inCorrectQuestions,
      nonAttQueCount: this.skippedQuestions,
      corrQueCount: this.correctQuestions,
      inCorrQueCount: this.inCorrectQuestions,
      timeTakenMin: this.totalMins,
      timeTakenSec: this.totalSecs,
      testType: this.testType,
      testMode: this.testMode,
      questions: JSON.stringify(this.questions),
      chapterList: this.chaptersList.toString(),
      manualCount: JSON.stringify(this.cntManual),
      phyStart: this.phyQuestionStarts,
      chemStart: this.chemQuestionStarts,
      mathsStart: this.mathsQuestionStarts,
      testSubmitted: true,
      questionStartsAt: 0,
    };
    // console.log("save test obj", this.testDetails);
    if (this.page == "unit-test" || this.page == "question-papers")
      this.saveTestInfoService.insertIntoTable(
        this.testDetails,
        this.loggedInType == "demo" ? "save_test_info_demo" : "save_test_info"
      );
    else if (this.page == "paused-test")
      this.saveTestInfoService.updateTable(
        this.testId,
        this.testDetails,
        this.loggedInType == "demo" ? "save_test_info_demo" : "save_test_info"
      );
  }

  saveUserAns() {
    let userAnsArray = [];
    this.questionsList.map((question) => {
      userAnsArray.push({
        testId: this.testId,
        queId: question.QUESTION_ID,
        userAns: question.userAns,
        userNumericalAns: question.userNumericalAns,
        userUnit: question.userUnit,
      });
    });
    // console.log("userAnsArray", userAnsArray);
    if (this.page == "unit-test" || this.page == "question-papers")
      this.viewTestInfoService.inserIntoTable(
        userAnsArray,
        this.loggedInType == "demo" ? "view_test_info_demo" : "view_test_info"
      );
    else if (this.page == "paused-test")
      this.viewTestInfoService.updateTable(
        userAnsArray,
        this.loggedInType == "demo" ? "view_test_info_demo" : "view_test_info"
      );
  }

  clear() {
    if (this.questionsList[this.index].QUESTION_TYPE != 2) {
      this.selectionCleared = false;
      this.option = "";
      this.questionsList[this.index].userAns = "";
      this.selectedOption = this.selectedOptionArr.find(
        ({ questionNo }) =>
          questionNo === this.questionsList[this.index].QUESTION_ID
      );
      // console.log("selected in opt1", this.selectedOption);
      if (this.selectedOptionArr.includes(this.selectedOption)) {
        // console.log("in opt4 if");
        this.selectedOptionArr.splice(
          this.selectedOptionArr.indexOf(this.selectedOption),
          1
        );
      }
    } else if (this.questionsList[this.index].QUESTION_TYPE == 2) {
      this.answer = "";
      this.unit = "";
    }
  }

  async testInfo() {
    // if (this.testMode == 'manual') {
    //   this.questionCount = this.cntManual.cntAtt + this.cntManual.cntEasy + this.cntManual.cntHard + this.cntManual.cntMedium + this.cntManual.cntNonAtt + this.cntManual.cntNum + this.cntManual.cntPrevAsk + this.cntManual.cntTheory;
    // }
    const popover = await this.popoverController.create({
      component: TestInfoComponent,
      animated: true,
      backdropDismiss: true,
      cssClass: "myPopOver3",
      componentProps: {
        chapters: this.chaptersList,
        subjectName: this.subjectName,
        testMode: this.testMode,
        cntManual: this.cntManual,
        questionCount: this.questionCount,
        settings: this.settings,
        questions: this.questions,
      },
    });
    await popover.present();
  }

  async createToast(message, duration) {
    const toast = await this.toastController.create({
      animated: true,
      position: "middle",
      cssClass: "myToast",
      message: message,
      duration: duration,
    });
    await toast.present();
  }

  exitTest() {
    this.addRemoveBookmark();
    if (this.page == "unit-test" || this.page == "paused-test")
      this.router.navigateByUrl("/dashboard/unit-test");
    // else if (this.page == "saved-test" || this.page == "paused-test")
    //   this.router.navigateByUrl("/dashboard/view-test");
    // else if (this.page == "question-papers") {
    //   this.router.navigateByUrl("/dashboard/question-paper");
    // }
    else {
      this.navController.back();
    }
  }

  addRemoveBookmark() {
    if (this.page != "question-papers") {
      if (this.removedBookmarkQustions.length > 0) {
        let removeQuestionId = [];
        for (let i = 0; i < this.removedBookmarkQustions.length; i++) {
          removeQuestionId.push(this.removedBookmarkQustions[i].questionId);
        }
        this.questionBookmarkService.removeBookmarkQue(
          removeQuestionId,
          this.loggedInType == "demo"
            ? "question_bookmark_demo"
            : "question_bookmark"
        );
      }
      if (this.bookmarkQustions.length > 0) {
        this.questionBookmarkService.insertBookmarkQue(
          this.bookmarkQustions,
          this.bookmarkQustions.length,
          this.loggedInType == "demo"
            ? "question_bookmark_demo"
            : "question_bookmark"
        );
      }
    }
  }

  selectQuestion(index) {
    this.inCorrOption = "";
    this.testView = true;
    this.index = index;
    this.showTest();
    this.answer = this.questionsList[index].userNumericalAns;
    this.unit = this.questionsList[index].userUnit;
    this.option = this.questionsList[index].ANSWER;
    if (
      this.questionsList[this.index].ANSWER !=
      this.questionsList[this.index].userAns
    ) {
      this.inCorrOption = this.questionsList[this.index].userAns;
    }
    if (
      (this.page == "saved-test" ||
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers") &&
      this.testSubmitted == true
    ) {
      this.getQuestionStatus();
    }
  }

  solution() {
    this.inCorrOption = "";
    this.testView = true;
    this.index = 0;
    this.showTest();
    this.option = this.questionsList[0].ANSWER;
    if (
      this.questionsList[this.index].ANSWER !=
      this.questionsList[this.index].userAns
    ) {
      this.inCorrOption = this.questionsList[this.index].userAns;
    }
    if (
      (this.page == "saved-test" ||
        this.page == "unit-test" ||
        this.page == "paused-test" ||
        this.page == "question-papers") &&
      this.testSubmitted == true
    ) {
      this.getQuestionStatus();
    }
  }

  getQuestionStatus() {
    // console.log(this.questionsList[this.index]);
    if (this.questionsList[this.index].QUESTION_TYPE != 2) {
      if (this.questionsList[this.index].userAns == "") {
        this.questionStatus = "SKIPPED";
      } else if (
        this.questionsList[this.index].ANSWER ==
        this.questionsList[this.index].userAns
      ) {
        this.questionStatus = "CORRRECT";
      } else {
        this.questionStatus = "INCORRECT";
      }
    } else if (this.questionsList[this.index].QUESTION_TYPE == 2) {
      if (this.questionsList[this.index].userNumericalAns == "") {
        this.questionStatus = "SKIPPED";
      } else if (
        this.questionsList[this.index].userNumericalAns ==
          this.questionsList[this.index].NUMERICAL_ANSWER &&
        this.questionsList[this.index].userUnit ==
          this.questionsList[this.index].UNIT
      ) {
        this.questionStatus = "CORRRECT";
      } else {
        this.questionStatus = "INCORRECT";
      }
    }

    // if (
    //   (this.questionsList[this.index].QUESTION_TYPE != 2 &&
    //     this.questionsList[this.index].userAns == "") ||
    //   (this.questionsList[this.index].QUESTION_TYPE == 2 &&
    //     this.questionsList[this.index].userNumericalAns == "")
    // ) {
    //   this.questionStatus = "SKIPPED";
    // } else if (
    //   this.questionsList[this.index].ANSWER ==
    //     this.questionsList[this.index].userAns ||
    //   (this.questionsList[this.index].userNumericalAns ==
    //     this.questionsList[this.index].NUMERICAL_ANSWER &&
    //     this.questionsList[this.index].userUnit ==
    //       this.questionsList[this.index].UNIT)
    // ) {
    //   this.questionStatus = "CORRRECT";
    // } else {
    //   this.questionStatus = "INCORRECT";
    // }
  }

  async onPauseClick() {
    const alert = await this.alertController.create({
      subHeader: "Pause Test ?",
      message: "You can resume this test from Saved Test...!",
      cssClass: "alert-title",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
        },
        {
          text: "Confirm",
          handler: () => {
            this.pauseTest();
          },
        },
      ],
    });
    await alert.present();
  }

  pauseTest() {
    clearTimeout(this.stopClockInterval);
    this.getTestId();
    let day = new Date().getDate().toString();
    if (day.length == 1) day = "0" + day;
    let month = (new Date().getMonth() + 1).toString();
    if (month.length == 1) month = "0" + month;
    let year = new Date().getFullYear().toString();
    let submitDate = day + month + year;
    this.testDetails = {
      testDate: submitDate,
      subjectName: this.subjectName,
      totalQue: this.questionsList.length,
      corrMarks: this.settings.correctMarks,
      inCorrMarks: this.settings.inCorrectMarks,
      obtainedMarks: null,
      phyObtainedMarks: null,
      chemObtainedMarks: null,
      mathsObtainedMarks: null,
      totalMarks: this.outOfMarks,
      totalTime: this.settings.time,
      attQueCount: null,
      nonAttQueCount: null,
      corrQueCount: null,
      inCorrQueCount: null,
      timeTakenMin: this.fmins,
      timeTakenSec: this.fsecs,
      testType: this.testType,
      testMode: this.testMode,
      questions: JSON.stringify(this.questions),
      chapterList: this.chaptersList.toString(),
      manualCount: JSON.stringify(this.cntManual),
      phyStart: this.phyQuestionStarts,
      chemStart: this.chemQuestionStarts,
      mathsStart: this.mathsQuestionStarts,
      testSubmitted: false,
      questionStartsAt: this.index,
    };
    // console.log("save test obj", this.testDetails);
    this.addRemoveBookmark();
    this.saveTestInfoService
      .insertIntoTable(
        this.testDetails,
        this.loggedInType == "demo" ? "save_test_info_demo" : "save_test_info"
      )
      .then((_) => {
        if (this.page == "question-papers")
          this.router.navigateByUrl("/dashboard/question-paper");
        else this.router.navigateByUrl("/dashboard/unit-test");
      });
  }

  ngOnDestroy() {
    // this.status.backgroundColorByName("black");
  }
}
